<?php

/**
 * Class WC_Swish_Ecommerce_Method_Display_Widget
 *
 * Using Klarna's Payment method display you generate trust and increase your conversion.
 *
 * @class 		WC_Swish_Ecommerce_Method_Display_Widget
 * @version		1.0
 * @since		2.0
 * @category	Class
 * @author 		Krokedil
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class WC_Swish_Ecommerce_Method_Display_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'swish_pmd', // Base ID
			__( 'Swish Payment Method Display Widget', 'woocommerce-gateway-swish-ecommerce' ), // Name
			array( 'description' => __( 'Displays an image that informs the consumer about the payment methods that are available in your store.', 'woocommerce-gateway-swish-ecommerce' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		$design = ! empty( $instance['design'] ) ? $instance['design'] : 'short';
		$color  = ! empty( $instance['color'] ) ? $instance['color'] : 'blue';
		$width  = ! empty( $instance['width'] ) ? $instance['width'] : 440;

		echo "<img style='display:block;width:100%;height:auto;' src='https://cdn.klarna.com/1.0/shared/image/generic/badge/$klarna_locale/checkout/$design-$color.png?width=$width' />";

		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$design = ! empty( $instance['design'] ) ? $instance['design'] : '';
		$width  = ! empty( $instance['width'] ) ? $instance['width'] : '';
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'design' ) ); ?>"><?php _e( 'Design:' ); ?></label>
			<select class="" id="<?php echo esc_attr( $this->get_field_id( 'design' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'design' ) ); ?>">
				<option value="long" <?php selected( $design, 'long' ); ?>>Long</option>
				<option value="short" <?php selected( $design, 'short' ); ?>>Short</option>
			</select>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'width' ) ); ?>"><?php _e( 'Width (px):' ); ?></label> 
			<input class="" id="<?php echo esc_attr( $this->get_field_id( 'width' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'width' ) ); ?>" type="number" value="<?php echo esc_attr( $width ); ?>" min="100" />
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['design'] = ( 'long' == $new_instance['design'] || 'short' == $new_instance['design'] ) ? strip_tags( $new_instance['design'] ) : '';
		$instance['width'] = (int) $new_instance['width'];

		return $instance;
	}

}