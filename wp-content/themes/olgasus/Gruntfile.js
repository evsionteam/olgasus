module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        
        makepot: {
			target: {
				options: {
					mainFile:"./style.css",
					type: "wp-theme",
					domainPath:"./languages/"
				}
			}
		},


        // SASS COMPILE
        sass: {
            dist: {// Target
                options: {
                    style: 'compact'
                },
                files: {
                    './style.css': './assets/src/sass/style.scss',  // 'destination': 'source'
                    './assets/src/css/main.css': './assets/src/sass/main.scss'
                }
            }
        },
        
        // CSS AND JS CONCAT
        concat: {
            options:{
                separator:"\n \n /*** New File ***/ "
            },
            css: {
                src: [ 
                    './assets/src/css/main.css'
                ],
                dest: './assets/build/css/main.css'
            },
            js: { 
                src: [ 
                    './assets/src/js/wrapper/start.js',
                    './assets/src/js/nicescroll.3.6.8.min.js',
                    './assets/src/js/skip-link-focus-fix.js',
                    './assets/src/js/navigation.js',
                    './assets/src/js/jquery.bpopup.min.js',
                    './assets/src/js/jquery.carouFredSel-6.2.1-packed.js',
                    './assets/src/js/easyzoom.js',
                    // './assets/src/js/vslide.js',
                    './assets/src/js/main.js',
                    './assets/src/js/wrapper/end.js'
                ],
                dest: './assets/build/js/main.js'
            }

        },

        // MINIFIE CSS
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                keepSpecialComments: 0,
                sourceMap: false
            },
            target: {
                files: [{
                        expand: true,
                        cwd: './assets/src/css/',
                        src: [ '*.css', '!*.min.css' ],
                        dest: './assets/build/css/',
                        ext: '.min.css'
                    }]
            }
        },
        
        // MIINIFY JS
        uglify: {
            options: {
                    report: 'gzip'
            },
            main: {
                    src: [ './assets/build/js/main.js' ],
                    dest: './assets/build/js/main.min.js'
            }
        },
        
        // COPY CONTENT
        copy: {
            img: {
              files: [
                        { expand: true, cwd: './assets/src/img', src: '**', dest: './assets/build/img/', filter: 'isFile'}
                    ]
            }
        },
                
        // GRUNT WATCH
        watch: {
            
            sass: {
                files: 
                [
                    './assets/src/sass/*.scss', 
                    './assets/src/sass/**/*.scss', 
                    '../assets/src/sass/**/**/*.scss'
                ],
                tasks: [ 'css' ]
            },
            
            js: {
                files: 
                [
                    './assets/src/js/*.js', 
                    './assets/src/js/**/*.js', 
                ],
                tasks: [ 'js' ]
            }
            
        }

    });

    grunt.loadNpmTasks( 'grunt-contrib-sass' );
    grunt.loadNpmTasks( 'grunt-contrib-cssmin' );
    grunt.loadNpmTasks( 'grunt-contrib-concat' );
    grunt.loadNpmTasks( 'grunt-contrib-uglify' );
    grunt.loadNpmTasks( 'grunt-contrib-watch' );
    grunt.loadNpmTasks( 'grunt-contrib-copy' );
    grunt.loadNpmTasks("grunt-wp-i18n");

    //register grunt default task
    grunt.registerTask( 'css', [ 'sass', 'concat', 'cssmin' ] );
    grunt.registerTask( 'js', [ 'concat', 'uglify' ] );
    grunt.registerTask( 'pot', ['makepot'] );

    grunt.registerTask( 'default', [ 'sass', 'concat', 'uglify', 'cssmin', 'copy' ] );
}