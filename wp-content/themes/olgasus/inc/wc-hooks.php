<?php

/* Add currency for Krona */
add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);
function change_existing_currency_symbol( $currency_symbol, $currency ) {

    $currency = strtolower($currency);

    if ( $currency ==  'kr' || $currency =='sek'){
        $currency_symbol = ':-';
    }

    return $currency_symbol;
}

/* Remove dropdown from wc prodect page */
add_action('init','wc_remove_dropdown');
function wc_remove_dropdown(){
    remove_action('woocommerce_before_shop_loop','woocommerce_catalog_ordering',30);
}

/* Remove Search result in wc product page */
add_action('init','wc_remove_search_result');
function wc_remove_search_result(){
    remove_action('woocommerce_before_shop_loop','woocommerce_result_count',20);
}

/* 5 products per row */
add_filter('loop_shop_columns', 'loop_columns', 999);
function loop_columns() { return 5; }

/* Remove breadcrumbs */
add_action( 'init', 'olgasus_remove_wc_breadcrumbs' );
function olgasus_remove_wc_breadcrumbs() {
    remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
}

/* Product category page title styling */
add_action('woocommerce_before_main_content','add_div_open_before_title',11);
function add_div_open_before_title() {    
    global $wp_query;
        if(!is_product()){
            echo '<div class="inner-page-title">';
            $cat = $wp_query->get_queried_object();
            if( !empty( $cat->term_id ) ){

                $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
                if ( $thumbnail_id ) { 
                 $image = wp_get_attachment_url( $thumbnail_id );
                } else {
                    $image = wc_placeholder_img_src();
                }
                echo '<img src="'.$image.'" >';
            }
        }    
}

add_action('woocommerce_archive_description','add_div_close_after_title',9);
function add_div_close_after_title() {
    if(!is_product()){
        echo '</div><!-- inner-page-title -->';
    }
}

/* Add to cart button remove (shop page) */
remove_action('woocommerce_after_shop_loop_item','woocommerce_template_loop_add_to_cart',10);

/* Add figure tag in the product loop with close and open tags (shop page) */
add_action( 'woocommerce_before_shop_loop_item', 'olgasus_add_figure_open', 20 );
function olgasus_add_figure_open(){
    echo '<figure id="image-slide">';
    global $product;
    $attachment_ids = $product->get_gallery_attachment_ids();
    if($attachment_ids!=false){
        $gallery_first_img = wp_get_attachment_image_src($attachment_ids[0],'shop_catalog', true);
        $next_img = $gallery_first_img[0];
        echo '<div class="hidden-image"><a href="'.get_the_permalink().'"><img src= '.$next_img.' ></a></div>';
    }

}

add_action( 'woocommerce_before_shop_loop_item_title', 'olgasus_add_figure_close', 20 );
function olgasus_add_figure_close(){
    echo '</figure><!-- #figure-->';
}


/* Add cart icon with add-to-cart feature */
add_action( 'woocommerce_before_shop_loop_item_title', 'olgasus_add_cart_btn', 15 );
function olgasus_add_cart_btn( $args = array() ){
    global $product;

    if ( $product ) {
        $defaults = array(
            'quantity' => 1,
            'class'    => implode( ' ', array_filter( array(
                    'button',
                    'product_type_' . $product->product_type,
                    $product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
                    $product->supports( 'ajax_add_to_cart' ) ? 'ajax_add_to_cart' : ''
            ) ) )
        );

        $args = apply_filters( 'woocommerce_loop_add_to_cart_args', wp_parse_args( $args, $defaults ), $product );

    }
    ?>
    <figcaption>
        <a href="<?php echo esc_url( $product->add_to_cart_url() ); ?>" class="quickview" data-product-id="<?php echo $product->id  ?>">
            <i class="fa fa-expand"></i>
        </a>
        <?php wc_get_template( 'loop/add-to-cart.php', $args ); ?>
    </figcaption>
    <?php
}


/* Parent anchor open-tag removed in product loop */
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
/* Parent anchor close-tag removed in product loop  */
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
/* Get the title removed in product loop  */
remove_action( 'woocommerce_shop_loop_item_title','woocommerce_template_loop_product_title',10 );


/* Add link with product title in product loop */
add_action('woocommerce_shop_loop_item_title','olgasus_add_title_with_link',10);
function olgasus_add_title_with_link(){
    $id=get_the_ID();
    $olgasus_product_data = wp_get_object_terms($id,'brand');
    foreach($olgasus_product_data as $product){
       echo '<h1><a href="'. get_the_permalink().'">'.$product->name.'</a></h1>';
    }
    echo '<h3><a href="'.get_the_permalink().'">' . get_the_title() . '</a></h3>';
}

/* Add filter row in top bar */
add_filter( 'wc_add_to_cart_params', 'modidify_view_cart_text' );
function modidify_view_cart_text( $data ){
    $data['i18n_view_cart'] = "<i class='fa fa-check'></i>";
    return $data;
}

/* Add filter row in top bar */
add_action('woocommerce_archive_description','olgasus_wc_add_filter_bar',10);
function olgasus_wc_add_filter_bar(){
    dynamic_sidebar('wc-archive-bar');
}

/* Wrap all contents of single product with div(single-product page)*/
add_action( 'woocommerce_before_single_product','wrap_single_page_content_div_open',9 );
function wrap_single_page_content_div_open(){
    if(is_product()){
        echo '<div class="page-content">';
    }    
}

add_action( 'woocommerce_after_main_content','wrap_single_page_content_div_close',9 );
function wrap_single_page_content_div_close(){
    if(is_product()){
        echo '</div> <!-- page-content -->';
    }    
}

/* Product excert moved below category and tags */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );

add_action( 'woocommerce_single_product_summary','woocommerce_template_single_excerpt',28);
add_action( 'woocommerce_before_single_variation','woocommerce_template_single_excerpt', 15 );


/* remove reviews tab from additional section*/
add_filter( 'woocommerce_product_tabs', 'olgasus_remove_reviews_tab', 98 );
function olgasus_remove_reviews_tab($tabs) {
    unset($tabs['additional_information']);
    unset($tabs['description']);
    return $tabs;
}

/* Adding a tab in product page for leverans content */
add_filter( 'woocommerce_product_tabs', 'olgasus_product_tab' );
function olgasus_product_tab( $tabs ) {
    // Adds the new tab
    $tabs['leverans'] = array(
        'title'     => __( 'LEVERANS', 'woocommerce' ),
        'priority'  => 10,
        'callback'  => 'olgasus_product_tab_content'
    );
    $tabs['market'] = array(
        'title'     => __( 'MÄRKET', 'woocommerce' ),
        'priority'  => 15,
        'callback'  => 'olgasus_product_tab_content'
    );
    return $tabs;
}

function olgasus_product_tab_content($key) {
    global $olgasus;
    $content = '';

    switch($key){
        case 'leverans':
            if($olgasus['leverans-content']):
                $content = $olgasus['leverans-content'];
            endif;
        break;

        case 'market':

            global $product;

            $terms = get_the_terms( $product->id , 'brand' );

            if( false != $terms && !is_wp_error( $terms ) ){
                    $data = $terms[0];
                    $thumbnail_id = get_woocommerce_term_meta( $data->term_id, 'thumbnail_id', true );
                    if ( false != $thumbnail_id ) {
                        $image = wp_get_attachment_image( $thumbnail_id ,'medium');
                    ?>
                    <a href="<?php echo get_category_link( $data->term_id ); ?>" > 
                       <?php echo $image; ?>
                    </a>
                    <br>

                    <?php
                   echo wpautop( $data->description );
                }
            }
        break;

        default:
            $content = '';
    }

    echo wpautop($content);
}

/* Adding title on top of the drop down menu */
add_filter( 'woocommerce_dropdown_variation_attribute_options_args', 'olgasus_single_product_filter_dropdown_args', 10 );
function olgasus_single_product_filter_dropdown_args( $args ) {
    $variation_tax = get_taxonomy( $args['attribute'] );
    
    if(!empty($variation_tax)){
        $args['show_option_none'] = apply_filters( 'the_title', $variation_tax->labels->name );
    }
    return $args;
}

/* Model info shown below the add to cart  button in single-product page */
add_action( 'woocommerce_product_meta_start', 'olgasus_single_page_variation_info_block',10);
function olgasus_single_page_variation_info_block(){
    global $product;
    if( $product->is_type( 'variable' ) ){
        $attributes = $product->get_attributes();
        $model = array_filter($attributes,function($var){
            return ( $var['is_taxonomy'] && $var['name']==='pa_modellen' );
        });

        if(isset($model['pa_modellen'])){
            $values = wc_get_product_terms( $product->id, $model['pa_modellen']['name'], array( 'fields' => 'names' ) );
            echo '<span class="attributes">'.strip_tags(apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $model['pa_modellen'], $values )).'</span>';    
        }
    }
}

/* WP share plugin for woocommerce social link icons */
add_action( 'woocommerce_share', 'olgasus_all_share' );
function olgasus_all_share(){
    echo do_shortcode( '[wp_allshare theme="theme4" networks="facebook,twitter,pinterest,email" /]' );
} 

/* Brand name in single product page */
add_action( 'woocommerce_single_product_summary', 'olgasus_single_product_category_name', 4 );
function olgasus_single_product_category_name(){
    $id=get_the_ID();
    $olgasus_product_data = wp_get_object_terms($id,'brand');

    foreach($olgasus_product_data as $product_name){
       echo '<h1>'.$product_name->name.'</h1>';
    }
    
}

add_action( 'woocommerce_before_main_content', 'add_product_popup' );
function add_product_popup(){
    
    if( is_shop() || is_product_category() || is_product() ){
        get_template_part( 'template-parts/content-product', 'popup' );
    }
}

/* Remove up sell section from cart page (cart-page)*/
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );

/* Remove proceed to checkout button (cart-page)*/
// remove_action('woocommerce_proceed_to_checkout','woocommerce_button_proceed_to_checkout', 20);

/* Add custom proceed to checkout button with payment method option (cart-page) */
// add_action('woocommerce_proceed_to_checkout','olgasus_payment_method_option',20);
function olgasus_payment_method_option(){
    ?>
    <div class="payment-method">
        <h2>Betalningsmetod</h2>
        <?php
        global $woocommerce, $klarna_checkout_url;
        $checkout_url = $woocommerce->cart->get_checkout_url();
       
        ?>
        <form name="olgasus-checkout-select" method="POST">
            
            <div>
                <input id="klarna-pymnt" type="radio" name="payment" value="<?php echo esc_url($klarna_checkout_url) ?>" checked>
                <label for="klarna-pymnt">
                    <!-- <img src="<?php //echo get_template_directory_uri() . '/assets/src/img/klarna.png' ?>"/> -->
                    <?php _e( "Klarna", 'olgasus' ); ?>
                </label>
            </div>
            <div>
                <button type="submit" class="btn btn-default">
                    <?php _e('Proceed to Checkout', 'olgasus') ?>
                </button>
            </div>
        </form>
    </div><!-- /.payment-method --> 

    <?php
}

/*remove default image loop in archive page*/
remove_action('woocommerce_before_shop_loop_item_title','woocommerce_template_loop_product_thumbnail',10);
/* Make image clickable */
add_action('woocommerce_before_shop_loop_item_title','woocommerce_template_loop_product_thumbnail',10);
function woocommerce_template_loop_product_thumbnail(){
    echo '<a href="'.get_the_permalink().'">'.woocommerce_get_product_thumbnail().'</a>';
}

/* Remove price from archive page */
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );

/* Add price with single page link */
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
function woocommerce_template_loop_price (){
    global $product;
    if ( $price_html = $product->get_price_html() ) : ?>
        <span class="price"><a href="<?php  echo get_page_link($product->id); ?> "><?php echo $price_html; ?></a></span>
    <?php endif;

}

/* Meta fields for title of upsell product and related products */
// Display Fields
add_action( 'woocommerce_product_options_general_product_data', 'olgasus_add_custom_general_fields' );
function olgasus_add_custom_general_fields() {
    
    global $woocommerce, $post;
    echo '<div class="options_group">';
    // Custom fields will be created here...
    // Text Field
    woocommerce_wp_text_input( 
        array( 
            'id'          => '_you_may_also_like', 
            'label'       => __( 'Up Sell Product Title', 'woocommerce' ), 
            'placeholder' => 'Up Sell Product Title',
            'desc_tip'    => 'true',
            'description' => __( 'Enter the title to show up-sell product in single product page.', 'woocommerce' ) 
        )
    );

    // Text Field
    woocommerce_wp_text_input( 
        array( 
            'id'          => '_related_product', 
            'label'       => __( 'Related Product Title', 'woocommerce' ), 
            'placeholder' => 'Related Product Title',
            'desc_tip'    => 'true',
            'description' => __( 'Enter the title to show related product in single product page.', 'woocommerce' ) 
        )
    );
  
    echo '</div>';
    
}

// Save Fields
add_action( 'woocommerce_process_product_meta', 'olgasus_add_custom_general_fields_save' );
function olgasus_add_custom_general_fields_save( $post_id ){
    
    // Text Field
    $woocommerce_text_field = $_POST['_you_may_also_like'];
        update_post_meta( $post_id, '_you_may_also_like', esc_attr( $woocommerce_text_field ) );
    
    // Text Field
    $woocommerce_text_field = $_POST['_related_product'];
        update_post_meta( $post_id, '_related_product', esc_attr( $woocommerce_text_field ) );
    
}

//show attributes after summary in product single view
add_action('woocommerce_single_product_summary','product_attributes_in_archive', 40);
function product_attributes_in_archive() {
    global $product;
    $attributes =$product->get_attributes();
    if( false != $attributes ){
        echo '<div class="product_meta_attributes">';
        $total = count($attributes);
        $i=0;
       
        foreach ( $attributes as $attribute ) {
            $i++;
            if( $attribute['is_visible'] ) {
                $taxo =  get_taxonomy( $attribute['name'] );
                echo '<div class="attribute_data">';
                echo '<span>' . ucfirst( $taxo->label ) . ': ' . get_the_term_list($product->ID, $attribute['name'], '<span class="term-list sku">', ', </span><span class="term-list sku">', '</span>' );
                echo '</span></div>';                
            }
            /*if ($i != $total) echo', ';*/
        }
        echo '</div>';
    }
}

/* add the filter (remove added to cart message) */
add_filter( 'wc_add_to_cart_message', 'empty_wc_add_to_cart_message', 10, 2 );
function empty_wc_add_to_cart_message( $message, $product_id ) { 
    return ''; 
}; 
 
/* Number of products per page */         
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 30;' ), 20 );


/* Change price format from range to from in product variation */
add_filter( 'woocommerce_variable_sale_price_html', 'olgasus_variable_price_format', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'olgasus_variable_price_format', 10, 2 );
function olgasus_variable_price_format( $price, $product ) {
 
    $prefix = sprintf('%s ', __('fr.', 'olgasus'));
 
    $min_price_regular = $product->get_variation_regular_price( 'min', true );
    $min_price_sale    = $product->get_variation_sale_price( 'min', true );
    $max_price = $product->get_variation_price( 'max', true );
    $min_price = $product->get_variation_price( 'min', true );
 
    $price = ( $min_price_sale == $min_price_regular ) ?
        wc_price( $min_price_regular ) :
        '<ins>fr. ' . wc_price( $min_price_sale ) . '</ins>'.' <del>' . wc_price( $min_price_regular ) . '</del>' ;
 
    return ( $min_price == $max_price ) ? $price : sprintf('%s%s', $prefix, $price);
 
}

/* add price info under variation table*/
add_action( 'woocommerce_single_variation','rh_variation_price_wrapper_start',4 );
function rh_variation_price_wrapper_start(){
    echo "<div class='rh-variation-price'>";
}
add_action( 'woocommerce_single_variation','woocommerce_template_single_price',5 );
add_action( 'woocommerce_single_variation','rh_variation_price_wrapper_end',6 );
function rh_variation_price_wrapper_end(){
    echo "</div>";
}
/**/ 

add_filter( 'woocommerce_show_variation_price', '__return_true' );


add_filter( 'woocommerce_variation_is_active', 'disable_unavailable_variations', 10, 2 );

function disable_unavailable_variations( $grey_out, $variation ) {

    $price = $variation->get_price();

    if ( ! $variation->is_in_stock() || empty($price) ){
        return false;
    }
    return true;
}