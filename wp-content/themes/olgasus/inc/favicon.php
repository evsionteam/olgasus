<?php 
add_filter( 'get_site_icon_url', 'olgasus_provide_site_icon',10,2);
function olgasus_provide_site_icon($url, $size){
    global $olgasus;
    $fav_sizes = array(16,32,96);
    $apple_sizes = array(57,60,72,76,114,120,144,152,180,192);
    if( in_array($size, $fav_sizes) ){
        $url = ( isset($olgasus['favicon-'.$size]['url']) && !empty($olgasus['favicon-'.$size]['url']) )? $olgasus['favicon-'.$size]['url']:'';
    }else if( in_array($size, $apple_sizes) ){
        $url = ( isset($olgasus['apple-icon-'.$size]['url']) && !empty($olgasus['apple-icon-'.$size]['url']) )? $olgasus['apple-icon-'.$size]['url']: '';
    }
    return $url;
}
add_filter( 'site_icon_image_sizes', 'olgasus_site_icon_size' );
function olgasus_site_icon_size( $sizes ) {    
    $fav_sizes = array(16,32,57,60,72,76,96,114,120,144,152,180,192);
    $sizes = array_merge($sizes, $fav_sizes);
    return $sizes;
}
add_action('wp_head','olgasus_site_icon_tag');
function olgasus_site_icon_tag() {
    global $olgasus;
    $meta_tags[] = sprintf( '<link rel="icon" type="image/png" sizes="16x16" href="%s"/>', esc_url( get_site_icon_url( 16 ) ) );
    $meta_tags[] = sprintf( '<link rel="icon" type="image/png" sizes="32x32" href="%s"/>', esc_url( get_site_icon_url( 32 ) ) );
    $meta_tags[] = sprintf( '<link rel="icon" type="image/png" sizes="96x96" href="%s"/>', esc_url( get_site_icon_url( 96 ) ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="57x57" href="%s" />', get_site_icon_url( 57 ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="60x60" href="%s" />', get_site_icon_url( 60 ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="72x72" href="%s" />', get_site_icon_url( 72 ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="76x76" href="%s" />', get_site_icon_url( 76 ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="114x114" href="%s" />', get_site_icon_url( 114 ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="120x120" href="%s" />', get_site_icon_url( 120 ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="144x144" href="%s" />', get_site_icon_url( 144 ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="152x152" href="%s" />', get_site_icon_url( 152 ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="180x180" href="%s" />', get_site_icon_url( 180 ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="192x192" href="%s" />', get_site_icon_url( 192 ) );   
    $meta_tags[] = sprintf( '<meta name="msapplication-TileImage" content="%2s">', esc_url( get_site_icon_url( 144 ) ) ); 
        
    if( isset($olgasus['favicon-theme-color']) && !empty($olgasus['favicon-theme-color']) ){
        $meta_tags[] = '<meta name="theme-color" content="'.$olgasus['favicon-theme-color'].'">';
    }
    if( isset($olgasus['favicon-title-color']) && !empty($olgasus['favicon-title-color']) ){
        $meta_tags[] = '<meta name="msapplication-TileColor" content="'.$olgasus['favicon-title-color'].'">';
    }
    echo implode("\n",$meta_tags);
}