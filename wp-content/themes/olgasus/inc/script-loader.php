<?php
function olgasus_scripts() {
  $ver = '1.0.0';
  $ver = time();
  $min = '.min';
  if( defined('SCRIPT_DEBUG' ) && SCRIPT_DEBUG === true ) {
      $min = '';
  }

  wp_enqueue_style( 'olgasus-style', get_stylesheet_uri(), array( 'wp-jquery-ui-dialog' ) );
  wp_enqueue_script( 'olgasus', get_template_directory_uri() . '/assets/build/js/main' . $min . '.js', array('jquery','jquery-ui-dialog'), $ver, true );
  
  wp_enqueue_script( 'mordanizer', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js') ;
  
  $open_menu = 0;
    if( (isset( $_POST['add-to-cart'] ) && !empty($_POST['add-to-cart'] )) || (isset($_GET['removed_item']) && $_GET['removed_item'] == 1) ){
        $open_menu = 1;
    }
  
  $local_data = array( 'ajaxurl' => admin_url( 'admin-ajax.php' ),'open_menu' => $open_menu  );
  wp_localize_script( 'olgasus', 'localData', $local_data );

  $main_css = get_template_directory_uri() . '/assets/build/css/main'.$min.'.css';
  $map_script="";
  if(is_page(array(15))){
  $map_script= " function initMap() {
    var uluru = {lat: 58.2, lng:15};

    var map = new google.maps.Map(document.getElementById('olgasus-map'), {
        zoom: 8,
        scrollwheel: false,
        center: uluru,
        styles: [{
        'stylers': [{ 'saturation': -100 }]
        }]
    });

    var locations = [
        ['OLGAS HUS',57.777397,14.155820,2],
        ['OLGAS HUS',58.589622,16.185973,1]
      ];

      for(var i=0;i<locations.length;i++){
        
        var iconBase = 'http://localhost/newyorkgrill/wp-content/themes/nygrill/uploads/2017/02/map-marker-small.png';
        var marker = new google.maps.Marker({
          position:new google.maps.LatLng(locations[i][1],locations[i][2]),
          map:map,
          icon:iconBase,
        });
      }

  }

  var gmapScript = document.createElement('script');
  gmapScript.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyC0UFKb0FfidiD_2n4zolGgpeH47C0VMlQ&callback=initMap';
  gmapScript.type = 'text/javascript';
  gmapScript.async = 'true';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(gmapScript, s); ";
}
  $script = "
     var head  = document.getElementsByTagName('head')[0];
     var link  = document.createElement('link');
     link.rel  = 'stylesheet';
     link.type = 'text/css';
     link.href = '".$main_css."';
     link.media = 'all';
     head.appendChild(link);
  ";

  wp_add_inline_script( 'olgasus',$script.$map_script );

  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
      wp_enqueue_script( 'comment-reply' );
  }

   /* wp_deregister_script('wc-add-to-cart-variation'); 
    wp_dequeue_script ('wc-add-to-cart-variation'); 

   wp_register_script( 'wc-add-to-cart-variation', wcva_PLUGIN_URL. 'js/add-to-cart-variation2.js' ,array( 'jquery', 'wp-util' ), false, true);
   wp_enqueue_script('wc-add-to-cart-variation'); */
}

add_action( 'wp_enqueue_scripts', 'olgasus_scripts' );

add_action( 'admin_enqueue_scripts', 'olgasus_admin_script' );
function olgasus_admin_script(){
   wp_enqueue_media();
}