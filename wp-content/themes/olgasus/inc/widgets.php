<?php 

/**
 *Social link icon widget with text
 */
class olgasus_social_widget extends WP_Widget{

    public function __construct(){
        $widget_details = array(
            'classname' => 'olgasus_social_widget',
            'description' => 'Creates a featured item consisting of a description and social link icon.'
        );

        parent::__construct( 'olgasus_social_widget', 'Social Icon with dynamic description', $widget_details );
    }

    public function widget( $args, $instance ){ 
    	global $olgasus;

		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
		?>
		
		<div class='my-description'>
			<?php echo wpautop( esc_html( $instance['description'] ) ) ?>
		</div>

		<div class='social'>

			<?php if(!empty($olgasus['facebook-link'])){?>
			<a href='<?php echo $olgasus['facebook-link']; ?>' target='_blank' ><i class="fa fa-facebook-square" data-toggle="tooltip" data-placement="top" data-original-title="facebook"></i></a>
			<?php } ?>
			
			<?php if(!empty($olgasus['instagram-link'])){?>
			<a href='<?php echo $olgasus['instagram-link']; ?>' target='_blank' ><i class="fa fa-instagram"  data-toggle="tooltip" data-placement="top" data-original-title="instragram"></i></a>
			<?php } ?>

		</div>

		<?php

		echo $args['after_widget'];
    }

    public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'olgasus' );
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'olgasus' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>

		<?php
		$description = ! empty( $instance['description'] ) ? $instance['description'] : esc_html__( 'New Description', 'olgasus' );
		?>

		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>"><?php esc_attr_e( 'Description:', 'olgasus' ); ?></label> 

		<textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>" type="text" rows="4" cols="10"><?php echo esc_attr( $description ); ?> </textarea>
		</p>

		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['description'] = ( ! empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'] ) : '';
		return $instance;
	}
}

/**
 *Payment option widget with text
 */
class olgasus_payment_option_widget extends WP_Widget{

    public function __construct(){
        $widget_details = array(
            'classname' => 'olgasus_payment_option_widget',
            'description' => 'Selection of payment option.'
        );

        parent::__construct( 'olgasus_payment_option_widget', 
        		'Selection of payment option.', 
        		$widget_details );
    }

    public function widget( $args, $instance ){ 

    	global $olgasus, $woocommerce, $klarna_checkout_url;

    	$checkout_url = $woocommerce->cart->get_checkout_url();

		echo $args['before_widget'];

		//if ( ! empty( $instance['title'] ) ) {
		//	echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		//}
		?>
		<a href="<?php echo esc_url($checkout_url) ?>">
			<img src="<?php echo get_template_directory_uri() . '/assets/src/img/swish.png' ?>" alt="Swish"/>
		</a>

		<a href="<?php echo esc_url($klarna_checkout_url) ?>">
			<img src="<?php echo get_template_directory_uri() . '/assets/src/img/klarna.png' ?>" alt="Klarna"/>
		</a>
		
		<?php

		echo $args['after_widget'];
    }

    public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'olgasus' );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'olgasus' ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		
		return $instance;
	}
}
function olgasus_social_widget_init() {
	register_widget( 'olgasus_social_widget' );
	register_widget( 'olgasus_payment_option_widget' );
}
add_action('widgets_init','olgasus_social_widget_init');