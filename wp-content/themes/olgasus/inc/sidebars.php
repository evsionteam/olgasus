<?php

function olgasus_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'olgasus' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'olgasus' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 1', 'olgasus' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'olgasus' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 2', 'olgasus' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'olgasus' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 3', 'olgasus' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'olgasus' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 4', 'olgasus' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Add widgets here.', 'olgasus' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Woocommerce Archive Bar', 'olgasus' ),
		'id'            => 'wc-archive-bar',
		'description'   => esc_html__( 'This sidebar is displayed at the top of archive page of Woocommerce', 'olgasus' ),
		'before_widget' => '<section id="%1$s" class="wc-archive-bar widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'olgasus_widgets_init' );