<?php
add_action( 'init', 'olgasus_register_taxonomy' );
function olgasus_register_taxonomy(){

	$labels = array(
		'name'              => _x( 'Brands', 'Brand', 'olgasus' ),
		'singular_name'     => _x( 'Brand', 'Brand', 'olgasus' ),
		'search_items'      => __( 'Search Brands', 'olgasus' ),
		'all_items'         => __( 'All Brands', 'olgasus' ),
		'parent_item'       => __( 'Parent Brand', 'olgasus' ),
		'parent_item_colon' => __( 'Parent Brand:', 'olgasus' ),
		'edit_item'         => __( 'Edit Brand', 'olgasus' ),
		'update_item'       => __( 'Update Brand', 'olgasus' ),
		'add_new_item'      => __( 'Add New Brand', 'olgasus' ),
		'new_item_name'     => __( 'New Brand Name', 'olgasus' ),
		'menu_name'         => __( 'Brand', 'olgasus' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'brand' ),
	);
	register_taxonomy( 'brand', 'product', apply_filters('olgasus_woo_brand_arguments',$args ) );
}


/* add_action( 'brand_add_form_fields', 'add_brand_tax_image_field', 10 );
function add_brand_tax_image_field() { ?>
	<div class="form-field">

		<label for="series_image"><?php _e( 'Brand Image:', 'olgasus' ); ?></label>
		<input type="hidden" name="thumbnail_id" id="thumbnail_id" value="<?php echo $thumbnail_id; ?>" />
		<input type="text" name="series_image" id="series_image" class="series-image" value="">
		<input class="upload_image_button button" name="_add_series_image" id="_add_series_image" type="button" value="<?php _e('Select/Upload Image','olgasus');?>" />
		<script>
			jQuery(document).ready(function() {
				jQuery('#_add_series_image').on('click', function() {
					wp.media.editor.send.attachment = function(props, attachment) {
						jQuery('#series-img').attr("src",attachment.url);
						jQuery('#thumbnail_id').val(attachment.id);
						jQuery('.series-image').val(attachment.url);
					}
					wp.media.editor.open(this);
					return false;
				});
			});
		</script>
		
	</div>
<?php
}
*/
	
	add_action( 'brand_edit_form_fields', 'brand_edit_meta_field', 10);
	function brand_edit_meta_field($term) {

		$thumbnail_id = get_woocommerce_term_meta($term->term_id,'thumbnail_id',true);

		if ( $thumbnail_id ) {
			$image = wp_get_attachment_thumb_url( $thumbnail_id );
		} else {
			$image = wc_placeholder_img_src();
		}
		
		?>
		<tr class="form-field">

			<th scope="row" valign="top"><label for="_series_image"><?php _e( 'Brand Image', 'olgasus' ); ?></label></th>
			<td>
				<input type="hidden" name="thumbnail_id" id="thumbnail_id" value="<?php echo $thumbnail_id; ?>" />
				<input class="upload_image_button button" name="_series_image" id="_series_image" type="button" value="Select/Upload Image" />
			</td>
		</tr>
		<tr class="form-field">
		<th scope="row" valign="top"></th>
			<td style="height: 150px;">
				<style>
					div.img-wrap {
						/*background: url('http://placehold.it/960x300') no-repeat center;*/
						background-size:contain;
						max-width: 450px;
						max-height: 150px;
						width: 100%;
						height: 100%;
						overflow:hidden;
					}
					div.img-wrap img {
						max-width: 450px;
					}
				</style>
				<div class="img-wrap">
					<img src="<?php echo $image; ?>" id="series-img">
				</div>
				<script>
					'use strict';

					jQuery(document).ready(function() {
						jQuery('#_series_image').on('click', function() {
							wp.media.editor.send.attachment = function(props, attachment) {
								jQuery('#series-img').attr("src",attachment.url);
								jQuery('#thumbnail_id').val(attachment.id);								
							}
							wp.media.editor.open(this);
							return false;
						});
					});
				</script>
			</td>
		</tr>
	<?php
	}
	
	add_action( 'edited_brand', 'save_brand_custom_meta', 10, 2 );
	//add_action( 'create_brand', 'save_brand_custom_meta', 10, 2 );
	function save_brand_custom_meta( $term_id ) {
		if ( isset( $_POST['thumbnail_id'] ) ) {			
			update_term_meta($term_id, 'thumbnail_id', $_POST['thumbnail_id']);
		}	
	}

