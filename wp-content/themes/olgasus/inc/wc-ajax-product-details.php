<?php
/*ajax load products details on click*/
add_action('wp_ajax_olgasus_load_product_details', 'olgasus_load_product_details');
add_action('wp_ajax_nopriv_olgasus_load_product_details', 'olgasus_load_product_details');

function olgasus_load_product_details(){

    $output[ 'data' ] = '';
    $product_id = $_POST[ 'product_id' ];

    ob_start();

    if( !empty( $product_id ) ){

        $param = array( 
                    'p' => $product_id, 
                    'post_type' => 'product', 
                    'post_per_page' => 1 
                );

        $selected_product = new WP_Query( $param );

        while ($selected_product->have_posts()): 

            $selected_product->the_post();

            get_template_part( 'template-parts/wc-content-single', 'product' );

        endwhile;

        wp_reset_postdata();
    }

    $output['data'] = ob_get_clean();
    echo json_encode($output);
    die;
}
/**/