<?php

function ol_block($title,$content=null){
    extract(shortcode_atts(array(
            'title' => ''
            ),$title));

    $output = '';
    $output .= '<div class ="page-content"><div class="desc-holder">';
    $output .= '<h2 class="section-title">'.$title.'</h2>';
    $output .= '<div class ="desc-content">'.$content.'</div>';
    $output .= '</div></div>';
    return $output;
}

add_shortcode('ol_block','ol_block');  
