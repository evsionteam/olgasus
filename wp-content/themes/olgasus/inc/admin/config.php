<?php

    /**
     * ReduxFramework Barebones Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "olgasus";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Olgasus', 'olgasus' ),
        'page_title'           => __( 'Olgasus', 'olgasus' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => true,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '_options',
        // Page slug used to denote the panel
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!

        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        //'compiler'             => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    $args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'http://docs.reduxframework.com/',
        'title' => __( 'Documentation', 'redux-framework-demo' ),
    );

    $args['admin_bar_links'][] = array(
        //'id'    => 'redux-support',
        'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
        'title' => __( 'Support', 'redux-framework-demo' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-extensions',
        'href'  => 'reduxframework.com/extensions',
        'title' => __( 'Extensions', 'redux-framework-demo' ),
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
        'title' => 'Visit us on GitHub',
        'icon'  => 'el el-github'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://twitter.com/reduxframework',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://www.linkedin.com/company/redux-framework',
        'title' => 'Find us on LinkedIn',
        'icon'  => 'el el-linkedin'
    );

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'redux-framework-demo' ), $v );
    } else {
        $args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'redux-framework-demo' );
    }

    // Add content after the form.
    $args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'redux-framework-demo' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

    // -> START Basic Fields


    /*SITE INFORMATION*/
    Redux::setSection( $opt_name, array(
        'title'  => __( 'HomePage Section', 'redux-framework-demo' ),
        'id'     => 'home-basic',
        'desc'   => __( 'Homepage section.', 'redux-framework-demo' ),
        'icon'   => 'el el-home'
    ) );
    

    Redux::setSection( $opt_name, array(
        'title'      => __( 'HomePage Section', 'redux-framework-demo' ),
        'desc'       => __( 'Enter Details of the banner in homepage section'),
        'id'         => 'opt-text-subsection',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'site-logo',
                'type'     => 'media',
                'title'    => __( 'Site Logo', 'redux-framework-demo' ),
                /*'subtitle' => __( 'Subtitle', 'redux-framework-demo' ),*/
                'desc'     => __( 'Insert Site Logo', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'banner-title',
                'type'     => 'text',
                'title'    => __( 'Banner Title', 'redux-framework-demo' ),
                /*'subtitle' => __( 'Subtitle', 'redux-framework-demo' ),*/
                'desc'     => __( 'Enter Title Text for Banner Image', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'banner-sub-title',
                'type'     => 'textarea',
                'title'    => __( 'Banner Sub Title', 'redux-framework-demo' ),
                /*'subtitle' => __( 'Subtitle', 'redux-framework-demo' ),*/
                'desc'     => __( 'Enter Sub Title Text for Banner Image', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'banner-image',
                'type'     => 'media',
                'title'    => __( 'Banner Image', 'redux-framework-demo' ),
                /*'subtitle' => __( 'Subtitle', 'redux-framework-demo' ),*/
                'desc'     => __( 'Insert Banner Image', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'home-insta-textarea',
                'type'     => 'textarea',
                'title'    => __( 'Instagram Description', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'home-contact-form',
                'type'     => 'text',
                'title'    => __( 'Contact Form Shortcode', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'footer-text',
                'type'     => 'text',
                'title'    => __( 'Footer Text', 'redux-framework-demo' ),
                /*'subtitle' => __( 'Subtitle', 'redux-framework-demo' ),*/
                'desc'     => __( 'Text to display in the footer', 'redux-framework-demo' ),
            ),
        )
    ) );

    /* #SITE INFORMATION*/


    /*FAVICON*/
      Redux::setSection( $opt_name, array(
        'title'  => __( 'Favicon Image', 'redux-framework-demo' ),
        'id'     => 'basic',
        'desc'   => __( 'Different favicons images for different devices.', 'redux-framework-demo' ),
        'icon'   => 'el el-home',
    ) );
     Redux::setSection( $opt_name, array(
        'title'      => __( 'Fav Icons', 'redux-framework-demo' ),
        'id'         => 'opt-favicon-subsection',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'favicon-16',
                'type'     => 'media',
                'title'    => __( 'Favicon (16x16)', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'favicon-32',
                'type'     => 'media',
                'title'    => __( 'Favicon (32x32)', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'favicon-96',
                'type'     => 'media',
                'title'    => __( 'Favicon (96x96)', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'apple-icon-57',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (57x57)', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'apple-icon-60',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (60x60)', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'apple-icon-72',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (72x72)', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'apple-icon-76',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (76x76)', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'apple-icon-114',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (114x114)', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'apple-icon-120',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (120x120)', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'apple-icon-144',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (144x144)', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'apple-icon-152',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (152x152)', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'apple-icon-180',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (180x180)', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'android-icon-192',
                'type'     => 'media',
                'title'    => __( 'Android Icon (192x192)', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'favicon-theme-color',
                'type'     => 'color',
                'title'    => __('Favicon Background Color', 'redux-framework-demo'),
                'subtitle' => __('Pick a background color for Windows shortcut', 'redux-framework-demo'),
                'default'  => '#FFFFFF',
            ),
            array(
                'id'       => 'favicon-title-color',
                'type'     => 'color',
                'title'    => __('Favicon Title Color', 'redux-framework-demo'), 
                'subtitle' => __('Pick a title color for Windows shortcut', 'redux-framework-demo'),
                'default'  => '#333333',
            )
        )
    ) );
     /* #FAVICON*/ 

    Redux::setSection( $opt_name, array(
        'title'  => __( 'Services(homepage) Section', 'redux-framework-demo' ),
        'id'     => 'service-section',
        'icon'   => 'el el-home',
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Services(homepage)', 'redux-framework-demo' ),
        'id'         => 'service-content',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'field-i-title',
                'type'     => 'text',
                'title'    => __( 'Field 1 Title', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'field-i-content',
                'type'     => 'textarea',
                'title'    => __( 'Field 1 Content', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'field-ii-title',
                'type'     => 'text',
                'title'    => __( 'Field 2 Title', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'field-ii-content',
                'type'     => 'textarea',
                'title'    => __( 'Field 2 Content', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'field-iii-title',
                'type'     => 'text',
                'title'    => __( 'Field 3 Title', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'field-iii-content',
                'type'     => 'textarea',
                'title'    => __( 'Field 3 Content', 'redux-framework-demo' ),
            ),
        )
    ) );

    /*  CONTACT ADDRESSES*/
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Contact Section', 'redux-framework-demo' ),
        'id'     => 'address-section',
        'icon'   => 'el el-home',
    ) );


    Redux::setSection( $opt_name, array(
        'title'      => __( 'Jonkopping', 'redux-framework-demo' ),
        'id'         => 'jonkopping-address-section-content',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'title-1',
                'type'     => 'text',
                'title'    => __( 'Adress Title', 'redux-framework-demo' ),
            ),
            
            array(
                'id'       => 'fulladdress-1',
                'type'     => 'textarea',
                'title'    => __( 'Full Address', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'timing-1',
                'type'     => 'textarea',
                'title'    => __( 'Bussiness Hour', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'telephone-1',
                'type'     => 'text',
                'title'    => __( 'Telephone Content Area', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'mail-1',
                'type'     => 'text',
                'title'    => __( 'Mail Content Area', 'redux-framework-demo' ),
            ),
        )
    ) );


    Redux::setSection( $opt_name, array(
        'title'      => __( 'Norrkoping', 'redux-framework-demo' ),
        'id'         => 'norkoping-address-section-content',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'title-2',
                'type'     => 'text',
                'title'    => __( 'Adress Title', 'redux-framework-demo' ),
            ),
            
            array(
                'id'       => 'fulladdress-2',
                'type'     => 'textarea',
                'title'    => __( 'Full Address', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'timing-2',
                'type'     => 'textarea',
                'title'    => __( 'Bussiness Hour', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'telephone-2',
                'type'     => 'text',
                'title'    => __( 'Telephone Content Area', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'mail-2',
                'type'     => 'text',
                'title'    => __( 'Mail Content Area', 'redux-framework-demo' ),
            ),
        )
    ) );

    
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Social Links', 'redux-framework-demo' ),
        'id'         => 'social-footer',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'facebook-link',
                'type'     => 'text',
                'title'    => __( 'Facebook Link', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'instagram-link',
                'type'     => 'text',
                'title'    => __( 'Instagram Link', 'redux-framework-demo' ),
            ),
        )
    ) );

    /* WooCommerce leverans part content*/
    Redux::setSection( $opt_name, array(
        'title'  => __( 'WooCommerce Section', 'redux-framework-demo' ),
        'id'     => 'olgasus-wc-section',
        'icon'   => 'el el-home',
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'LEVERANS', 'redux-framework-demo' ),
        'id'         => 'olgasus-leverans-content',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'leverans-content',
                'type'     => 'textarea',
                'title'    => __( 'LEVERANS Content', 'redux-framework-demo' ),
            ),
        )
    ) );
