<?php

	add_shortcode( 'uz_glossary', 'uz_glossary' );
	
	function uz_glossary($atts){
		$atts = shortcode_atts( array(
			'block' 		=> 'list',
			'wrapper_class' => 'glosarry',
			'head_class' 	=> 'head',
			'type'			=> 'posts',
			'taxonomy'		=>  false,
			'post_type'		=>  'post'
		), $atts, 'uz_glossary' );


		$config = $atts;
		if($atts['block'] == 'div' ){
			$config['wrapper'] = 'div';
			$config['inner'] = 'div';
		}else{
			$config['wrapper'] = 'ul';
			$config['inner'] = 'li';
		}

		unset($config['block'] );

		return uz_get_glossary_data($config);
	}


	function uz_get_glossary_data($config){

		if( $config['type'] == 'terms' ){
			
			if(!$config['taxonomy']){
				return '';
			}

			$args = array(
		    	'taxonomy'   => $config['taxonomy'],
		        'hide_empty' => false,
		        'orderby'    => 'name',
		        'number'     => 0
		    );

		    $args = apply_filters( 'uz_glossary_get_data_args', $args, $config  );
			$data = get_terms($args);
			
		}else{
			if(!$config['post_type']){
				return '';
			}

			$args =  array(
			    'orderby'    		=> 'title',
			    'order' 			=> 'ASC',
			    'post_status' 		=> 'published',
			    'posts_per_page' 	=> -1,
			    'post_type'			=> $config['post_type']
			);

			$args = apply_filters( 'uz_glossary_get_data_args', $args, $config  );
			$data = get_posts($args);
		}

		return parse_results($data,$config);
	}


	function parse_results( $arrays, $config ){
		$lastChar = null;
		$charList = array();
		$output = '';
		$defaults = array(
			'wrapper' 		=> 'ul',
			'inner'			=> 'li',
			'wrapper_class' => 'glosarry',
			'head_class' 	=> 'head',
			'type'			=> 'posts',
			'taxonomy'		=>  false
		);

		$config = wp_parse_args( $config, $defaults );
		if( !is_wp_error( $arrays ) && count( $arrays ) > 0 ):
			foreach( $arrays as $val ) {
	            $list = '';
	            
	            if( $config['type'] == 'terms' ){
	            	$char = trim($val->name)[0];   
	            }else if( $config['type'] == 'posts' ){
	            	$char = trim($val->post_title)[0];
	            }else{
	            	$char = '';
	            }

	            if ( !ctype_alpha( $char ) ){
	                $char = '#';
	            }

	            if ( $char !== $lastChar ) {
	                $lastChar = $char;
	              
	                if ( $lastChar !== '' && !isset($charList[$char]) ){
	                    $charList[$char] = array();
	                	$list = '<'.$config['inner'].' class="'.$config['head_class'].'">' . strtoupper($char).'</'.$config['inner'].'>';
	                		$charList[$char][] = $list;
	                }
	            }

	            if( $config['type'] == 'terms' ){
	            	$list = '<'.$config['inner'].'><a href="'.get_term_link( $val, $config['taxonomy'] ).'">'.$val->name.'</a></'.$config['inner'].'>';
	            
	            }else if( $config['type'] == 'posts' ){
	            	$list = '<'.$config['inner'].'><a href="'.get_permalink( $val->ID ).'">'.$val->post_title.'</a></'.$config['inner'].'>';
	            
	            }else{
	            	if(is_array($val) || is_object($val)){
	            		return '';
	            	}
	            	$list = '<'.$config['inner'].'>'.$val.'</'.$config['inner'].'>';
	            }

	        	$list = apply_filters( 'uz_glossary_inner_block', $list, $config, $val );
	        	$charList[$char][] = $list;
	        }
	       
	        ksort($charList);

	        $lastindex = null;
	        foreach ($charList as $key => $value) {
	        	if ( $key !== $lastindex ) {
	        		$lastindex = $key;
	        		if ( $lastindex !== '' ){
	        			$output .= '<'.$config['wrapper'].' class="'.$config['wrapper_class'].'">';
	        		}
	        	}
				$output .= implode('', $value).'</'.$config['wrapper'].'>';        	
	        }
	        
	        //$output .= '</'.$config['wrapper'].'>';
        endif;
        return $output;
	}