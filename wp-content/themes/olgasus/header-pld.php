<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package olgasus
 */
global $olgasus;
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="preloader">
	<?php if ( is_front_page() ) : ?>
		<div class="icon-credit"><img src="<?php echo $olgasus['site-logo']['url']; ?>" alt="Olgashus" width='140'></div>
	<?php endif; ?>
</div>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'olgasus' ); ?></a>

	<header id="masthead" class="site-header" role="banner" data-fixed-after-touch>
		<!-- SITE NAME -->
		<div class="site-branding">
			<?php if(isset($olgasus['site-logo']['url'])): ?>
				<a href="<?php echo get_home_url(); ?>"><img src="<?php echo $olgasus['site-logo']['url']; ?>" alt="site-logo" /></a>
			<?php endif; ?>
		</div>
		<!-- #SITE NAME -->	

		<!-- small image -->
		<div class="site-branding-small">
			<?php if(isset($olgasus['site-logo']['url'])): ?>
				<a href="<?php echo get_home_url(); ?>"><img src="<?php echo $olgasus['site-logo']['url']; ?>" alt="site-logo" /></a>
			<?php endif; ?>
		</div>
		<!-- #small image -->

		<nav id="site-navigation" class="main-navigation affix-top" role="navigation">
			<!-- <?php if(isset($olgasus['site-logo']['url'])): ?>
				<img src="<?php echo $olgasus['site-logo']['url']; ?>" alt="site-logo" class="sec-logo" />
			<?php endif; ?> -->
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i class="fa fa-bars"></i></button>
			<?php wp_nav_menu( array( 
			'theme_location' => 'menu-1',
			 'menu_id' => 'primary-menu',
			 'menu_class'=>'menu'
			  ) ); ?>
		</nav><!-- #site-navigation -->
			
		<!-- MENU ICONS  -->
		<div class="header-accessories">
			<div class="ha-holder">
				<a href="javascript: void(0)" id="searchToggler"></a>
			</div>
			<div class="ha-holder">
				<a href="javascript: void(0)" id='accountToggler'>
					
					<div id="user-account">
						<ul class="my-account-list" href="#">
			                <li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"><?php _e('Mitt konto','olgasus'); ?></a></li>
			                <li><a href="<?php echo get_permalink( get_option('woocommerce_cart_page_id') ); ?>"><?php _e('Kassan','olgasus'); ?></a></li>
			                <!-- <li><a href="<?php //echo get_permalink( get_option('woocommerce_checkout_page_id') ); ?>"><?php //_e('Checkout','olgasus'); ?></a></li> -->
			            </ul>
			        </div>    
				</a>
				<!-- #MENU ICONS -->
		
			</div>
			<div class="ha-holder">
				<a href="javascript: void(0)" id="miniCartToggler">
				<span class="cart-counter"> <span class="cart-customlocation"><?php echo sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?></span></span></a>
			</div>
		</div>
		<!-- SIDE NAV -->
		<div id="olgasus-nav" class="side-nav">
			<div class="olgasus-nav-overlay"></div>
			<div class="olgasus-mini-cart">
				<div class="mini-cart-header">
					<h4><i class="fa fa-shopping-cart"></i> Varukorg</h4>
					<div id="mc-close">FORTSÄTT HANDLA</div>
				</div>
				<div id="olgasus-mini-cart-wrapper">
					<?php woocommerce_mini_cart(); ?>
				</div>	
			</div>
		</div>
		<!-- #SIDE NAV -->

		<div id="search-wrapper">
			<form role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<div class="search-group">
					<label class="screen-reader-text" for="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>"><?php _e( 'Search for:', 'woocommerce' ); ?></label>
					<input type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="search-field" placeholder="<?php echo esc_attr__( 'Search products&hellip;', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
					<!-- <input type="submit" value="<?php //echo esc_attr_x( 'Search', 'submit button', 'woocommerce' ); ?>" /> -->
					<button type="submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'woocommerce' ); ?>" ><i class="fa fa-search"></i></button>
					<input type="hidden" name="post_type" value="product" />
				</div>
			</form>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
