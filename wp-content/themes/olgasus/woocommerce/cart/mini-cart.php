<?php
/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/mini-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php do_action( 'woocommerce_before_mini_cart' ); ?>

<div class="shop_table cart">
	<form action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post" >
						
		<ul class="cart_list product_list_widget <?php echo $args['list_class']; ?>">

			<?php if ( ! WC()->cart->is_empty() ) : ?>

				<?php
					foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
						$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
						$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

						if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
							$product_name      = apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key );
							$thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
							$product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
							$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
							if(!empty($_product->get_sale_price())){
								$p_pr = '<span class = "mini-cart-mp">( '.$_product->get_regular_price().':- )</span>';
								$product_price_final = '<span class = "sp-color">'.$product_price.'</span>';
							} else{
								$p_pr = '';
								$product_price_final = $product_price;
							}
							?>
							<li class="<?php echo esc_attr( apply_filters( 'woocommerce_mini_cart_item_class', 'mini_cart_item', $cart_item, $cart_item_key ) ); ?>">

								<div class="mini-cart-list">
								<?php if ( ! $_product->is_visible() ) : ?>
									<?php echo str_replace( array( 'http:', 'https:' ), '', $thumbnail ) . $product_name . '&nbsp;'; ?>
										<?php else : ?>
											<a class="product-image" href="<?php echo esc_url( $product_permalink ); ?>">
												<?php echo str_replace( array( 'http:', 'https:' ), '', $thumbnail ) ; ?>
											</a>
										<?php endif; ?>
										<?php //echo WC()->cart->get_item_data( $cart_item ); ?>
										<?php $brands = wp_get_post_terms( $product_id, 'brand', array("fields" => "all") ); ?>
										<div class="product-details">
											<div class="product-basic-info">
												<?php 
													echo '<div class="product-name">'.$product_name . '&nbsp;</div>';
													
													if( !is_wp_error($brands) && !empty($brands) ){
														echo '<div class="minicart-brand-name">'.$brands[0]->name.'</div>';
													}
													
													echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<div class="quantity">' . sprintf( '%s %s', $product_price_final, $p_pr) . '</div>', $cart_item, $cart_item_key );
												?>
											</div>

											<div class="product-quantity" data-title="<?php _e( 'Quantity', 'woocommerce' ); ?>">
												<?php
													if ( $_product->is_sold_individually() ) {
														$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
													} else {
														$product_quantity = woocommerce_quantity_input( array(
															'input_name'  => "cart[{$cart_item_key}][qty]",
															'input_value' => $cart_item['quantity'],
															'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
															'min_value'   => '0'
														), $_product, false );
													}

													echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
													
												?>
											</div>

								
												<!-- <div class="spin"><span>&ndash;</span><input value="0" /><span>+</span></div> -->
								
										</div>
								</div>

								<?php
								echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
									'<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
									esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
									__( 'Remove this item', 'woocommerce' ),
									esc_attr( $product_id ),
									esc_attr( $_product->get_sku() )
								), $cart_item_key );
								?>
							</li>
							<?php
						}
					}
				?>

			<?php else : ?>

				<li class="empty"><?php _e( 'No products in the cart.', 'woocommerce' ); ?></li>

			<?php endif; ?>

		</ul><!-- end product list -->

	
	<?php wp_nonce_field( 'woocommerce-cart' ); ?>

	<?php if ( ! WC()->cart->is_empty() ) : ?>
		<div class="mini-cart-total clearfix">
			<div class="total"><?php _e( 'Att betala', 'woocommerce' ); ?>: <?php echo WC()->cart->get_cart_subtotal(); ?></div>

			<?php do_action( 'woocommerce_widget_shopping_cart_before_buttons' ); ?>

			<div class="buttons">

				<input type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Uppdatera', 'woocommerce' ); ?>" />
			
				<a href="<?php echo esc_url( wc_get_cart_url() ); ?>" class="button wc-forward"><?php _e( 'Till kassan', 'woocommerce' ); ?></a>
				<!-- <a href="<?php //echo esc_url( wc_get_checkout_url() ); ?>" class="button checkout wc-forward"><?php //_e( 'Checkout', 'woocommerce' ); ?></a> -->
			</div>
			
		</div>

	<?php endif; ?>
	</form>
</div>


<?php do_action( 'woocommerce_after_mini_cart' ); ?>