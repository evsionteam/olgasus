<?php

/**

 * olgasus functions and definitions

 *

 * @link https://developer.wordpress.org/themes/basics/theme-functions/

 *

 * @package olgasus

 */



if ( ! function_exists( 'olgasus_setup' ) ) :

/**

 * Sets up theme defaults and registers support for various WordPress features.

 *

 * Note that this function is hooked into the after_setup_theme hook, which

 * runs before the init hook. The init hook is too late for some features, such

 * as indicating support for post thumbnails.

 */

add_filter('woocommerce_get_availability', 'availability_filter_func');
function availability_filter_func($availability)
{
$availability['availability'] = str_ireplace('Slut i lager', 'Tillfälligt slut, kommer igen!', $availability['availability']);
return $availability;
}

function olgasus_setup() {

	/*

	 * Make theme available for translation.

	 * Translations can be filed in the /languages/ directory.

	 * If you're building a theme based on olgasus, use a find and replace

	 * to change 'olgasus' to the name of your theme in all the template files.

	 */

	load_theme_textdomain( 'olgasus', get_template_directory() . '/languages' );



	// Add default posts and comments RSS feed links to head.

	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'woocommerce' );

	add_theme_support( 'post-thumbnails' ); 



	/*

	 * Let WordPress manage the document title.

	 * By adding theme support, we declare that this theme does not use a

	 * hard-coded <title> tag in the document head, and expect WordPress to

	 * provide it for us.

	 */

	add_theme_support( 'title-tag' );



	/*

	 * Enable support for Post Thumbnails on posts and pages.

	 *

	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/

	 */

	add_theme_support( 'post-thumbnails' );

	set_post_thumbnail_size(150, 150, true);



	// This theme uses wp_nav_menu() in one location.

		( array(

		'menu-1' => esc_html__( 'Primary', 'olgasus' ),

	) );



	/*

	 * Switch default core markup for search form, comment form, and comments

	 * to output valid HTML5.

	 */

	add_theme_support( 'html5', array(

		'search-form',

		'comment-form',

		'comment-list',

		'gallery',

		'caption',

	) );



	// Set up the WordPress core custom background feature.

	add_theme_support( 'custom-background', apply_filters( 'olgasus_custom_background_args', array(

		'default-color' => 'ffffff',

		'default-image' => '',

	) ) );



	// Add theme support for selective refresh for widgets.

	add_theme_support( 'customize-selective-refresh-widgets' );



	add_image_size('olgasus_image_size',210,300);



}

endif;

add_action( 'after_setup_theme', 'olgasus_setup' );



/**

 * Set the content width in pixels, based on the theme's design and stylesheet.

 *

 * Priority 0 to make it available to lower priority callbacks.

 *

 * @global int $content_width

 */

function olgasus_content_width() {

	$GLOBALS['content_width'] = apply_filters( 'olgasus_content_width', 640 );

}

add_action( 'after_setup_theme', 'olgasus_content_width', 0 );





/**

 *Redux framework

 */

if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/inc/admin/ReduxCore/framework.php' ) ) {

    require_once( dirname( __FILE__ ) . '/inc/admin/ReduxCore/framework.php' );

}

if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/inc/admin/config.php' ) ) {

    require_once( dirname( __FILE__ ) . '/inc/admin/config.php' );

}









/*  Shortcodes  */

require __DIR__.'/inc/shortcode.php';



/* Favicons for site */

require __DIR__.'/inc/favicon.php';



/* All filters related to Woocommerces */

require __DIR__.'/inc/wc-hooks.php';



/* Register widget area. */

require __DIR__.'/inc/widgets.php';

/* Register Sidebar. */

require __DIR__.'/inc/sidebars.php';



/* Enqueue scripts and styles. */

require __DIR__.'/inc/script-loader.php';



/* Template tags for this theme. */

require __DIR__.'/inc/template-tags.php';



/* Functions that act independently of the theme templates. */

require __DIR__.'/inc/extras.php';



/* File that assist development work */

require __DIR__.'/inc/developer.php';



require __DIR__.'/inc/taxonomy.php';

require __DIR__.'/inc/wc-ajax-product-details.php';



require __DIR__. '/modules/uz-glossary/uz-glossary.php' ;

add_filter( 'loop_shop_per_page', 'olgashus_loop_shop_per_page', 20 );
function olgashus_loop_shop_per_page( $number ) {
  return 100;
}

