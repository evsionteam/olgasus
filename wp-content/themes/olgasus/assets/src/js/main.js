+jQuery(function($){

    // Preloader function
    function preloader(param) {
        this.time = param.time;
        this.init = function () {
            var time = this.time;
            $(window).load(function () {
                jQuery(".preloader").fadeOut();
                jQuery(".icon-credit").delay(time).fadeOut("slow");
            })
        }
    }

    function vslide( options ){

        this.opt = options;

        this.init = function(){

            var _this = this;

            if( null != this.opt.next ){

                $( document ).on( 'click', this.opt.next, function(){
                    // console.log('clicked');
                    if( _this.dec() ){

                        $( _this.opt.ele ).animate( { 'scrollTop': _this.opt.pos }, _this.duration );
                    }
                });
            }

            if( null != this.opt.prev ){

                $( document ).on( 'click', this.opt.prev, function(){
                    // console.log('clicked2');

                    if( _this.inc() ){

                        $( _this.opt.ele ).animate( { 'scrollTop': _this.opt.pos }, _this.duration );
                    }
                });
            }
        }

        this.inc = function(){

            //Stop scroll when it reaches to bottom
            if( this.opt.pos >= this.opt.ah - this.opt.vh ){
                return false;
            }

            this.opt.pos = this.opt.pos + this.opt.scroll;
            return true;
        }

        this.dec = function(){

            //Stop scroll when it is in top
            if(  this.opt.pos <= 0 ){
                return false;
            }

            this.opt.pos = this.opt.pos - this.opt.scroll;

            return true; 
        }

        this.init();
    };

    $.fn.vslide = function( options) {

        var defaults = {
                pos      : 0,    // Current Scroll Position
                scroll   : 200,  // Offset to scroll
                next     : null, // Next Button
                prev     : null, // Previous Button
                ele      : null, // Slider Element
                ah       : 0,    // Acutal Height of Slider
                vh       : 0,    // Visible Height of Slider
                duration : 300,  // Animation Duration 
                height   : 500   // Default Height for element if designer didn't gave height from css
            };

        var settings = $.extend( defaults, options );

        return this.each( function(){

            settings.ele = $( this );

            var vh = $( this ).height();

            // $( this ).css( { maxHeight: 'auto', height: 'auto', 'overflow' : 'hidden' } );

            var ah = jQuery(this)[0].scrollHeight;
            // console.log('ah: ' + ah +  ' vh ' + vh);

            if( typeof options.height != 'undefined' ){

                var h = parseInt( settings.height );

                if( isNaN( h ) ){
                    console.error( 'Invalid height for v slider.' );
                }else{

                    if( ah < h ){
                        console.log('well done');
                        $( settings.prev ).hide();
                        $( settings.next ).hide();
                    }
                    vh = h;
                }
                
            }else{

                if( vh == ah ){
                    //This means designer didn't gave height from css
                    //vh = ah/2;

                    console.warn( 'Set height for v slider.' );
                }

                //Hide prev and next button if slider is small for scrolling
                if( ah <= vh ){
                    console.log('well done');
                    $( settings.prev ).hide();
                    $( settings.next ).hide();
                }
            }

            $( this ).css( { height: vh } ); //Set previous height

            settings.ah = ah;
            settings.vh = vh;

            new vslide( settings );
            
        });
    };

    function changeProductThumbnail(){

        this.toggler = '.product-img-thumb';
        this.wrapper = '.easyzoom';

        this.apply = function(){

            var that = this;
            $easyZoom = jQuery('.easyzoom').easyZoom();

            jQuery( document ).on( 'click', this.toggler, function( e ){

                e.preventDefault();
                
                $( that.toggler ).not( this ).removeClass( 'active-img' );
                $( this ).addClass( 'active-img' );

                var largeImg = jQuery( this ).attr( 'data-large-img' );
                var mainImg  = jQuery( this ).attr( 'data-main-img' );

                var targetImg = jQuery( that.wrapper ).find( 'img' );
                var targetAnch = jQuery( that.wrapper ).find( 'a' );

                targetImg.attr( 'src', mainImg );
                targetImg.removeAttr( 'srcset' ).removeAttr( 'sizes' );
                targetAnch.attr( 'href', largeImg );
                targetAnch.css('background', ' #000 url("' + mainImg + '") top center /cover' );
                targetAnch.find('img').css('visibility', 'hidden');

                that.easyZoomTearDown();
            });
        }

        this.easyZoomTearDown = function(){
            var api = $easyZoom.data('easyZoom');

            if( 'undefined' !== typeof api ){
                api.teardown();
            }

            jQuery(this.wrapper).easyZoom();
        }
    }

    function productThumbnailSlider( isPopUp ){

        var element = 'thumbnails';

        if( null == document.getElementById( element ) ){
            return;
        }

        // if( typeof isPopUp == 'undefined' && $('#'+element + ' .product-img-thumb').length < 5 ){
        //     return;
        // }

        // if( typeof isPopUp != 'undefined' && $('#'+element + ' .product-img-thumb').length < 4 ){
        //     return;
        // }

        var options = {
            responsive: true,
            circular: true,
            infinite: false,
            auto    : false,
            width: "100%",
            items : {
                visible: {
                    min: 4,
                    max: 4
                }
            },
            prev    : {
                button  : "#product-thumb-prev",
                key     : "left"
            },
            next    : {
                button  : "#product-thumb-next",
                key     : "right"
            }
        }

        jQuery( '.product-thumbnail-paginattion' ).css( { 'display': 'block' } );

        jQuery( '.' + element ).carouFredSel( options ).css({'opacity':1});

    }
    
    // load product details
    function load_product_details(){

        this.loaderClass = 'loading';

        this.init = function(){

            var that = this;

            jQuery( '.quickview' ).on( 'click',function( e ){

                e.preventDefault();

                that.ele = jQuery( this );

                that.showLoader();

                that.productId = jQuery( this ).attr( 'data-product-id' );
                
                if( !isNaN( that.productId ) ){
                    that.ajax();
                }else{
                    that.removeLoader();
                }
            });
        }

        this.showLoader = function(){
            this.ele.addClass( this.loaderClass );
        }

        this.removeLoader = function(){
            this.ele.removeClass( this.loaderClass );
        }

        this.ajax = function(){

            var that = this;

            var onPost =  function( data ){

                    var dt = data,
                        content = $( '#ajax-product-content');

                    $( '#ajax-product-popup').bPopup({
                        speed: 350,
                        transition: 'ease',
                        positionStyle: 'fixed',
                        content: 'ajax',
                        onOpen: function() {
                            content.html( dt.data || '' );
                            // alert($('#ajax-product-popup img').length);
                            var lengimg = $('#ajax-product-popup img').length,
                             count = 0;

                            $easyZoom = jQuery('.easyzoom').easyZoom();
                            cpt.easyZoomTearDown();
                            $('#ajax-product-popup img').on('load', function(){
                                count++;
                                if(count == lengimg){
                                    $( '.thumbnails' ).vslide({
                                        next : '.next',
                                        prev : '.prev',
                                        scroll : 200,
                                        height: 480
                                    });
                                    $(".woocommerce-main-image, .product-img-thumb").each(function() {
                                        $(this).css('background', ' #000 url("' + $(this).find('img').attr('src') + '") top center /cover' );
                                        $(this).find('img').css('visibility', 'hidden');
                                    });
                                }
                            });

                        },
                        onClose: function() {
                            content.empty();
                        }
                    });

                }
            
            var onComplete = function(){
                that.removeLoader();
            }

            var ajaxOption = { 
                    'action'     : 'olgasus_load_product_details', 
                    'product_id' :  this.productId 
                };

            $.ajax({ 
                url: localData.ajaxurl,
                type: "POST",
                data: ajaxOption,
                success: onPost,
                dataType: "JSON",
                complete: onComplete 
            });
        }
    }

    // Make Background Image
    function makeBackgroundImg( param ){
        this.selector = param.selector,
        this.init = function(){
            var _this = this;
            _this.image = $(_this.selector).find('img');
            $(_this.selector).css("background-image", 'url(' + _this.image.attr("src") + ")");
            _this.image.css('display', 'none');
        }
    }

    //scrolltoTop Function
    function scrollToTop ( param ){
        this.markup   = null,
        this.selector = null;
        this.fixed    = true;
        this.visible  = false;

        this.init = function(){

            if( this.valid() ){

                if( typeof param != 'undefined' && typeof param.fixed != 'undefined' ){
                    this.fixed = param.fixed;
                }

                this.selector = ( param && param.selector ) ? param.selector : '#scrollToTop';

                this.getMarkup();
                var that = this;

                jQuery( 'body' ).append( this.markup );

                if( this.fixed ){

                    jQuery( this.selector ).hide();

                    var windowHeight = jQuery( window ).height();

                    jQuery( window ).scroll(function(){

                        var scrollPos     = jQuery( window ).scrollTop();

                        if(  ( scrollPos > ( windowHeight - 100 ) ) ){

                            if( false == that.visible ){
                                jQuery( that.selector ).fadeIn();
                                that.visible = true;
                            }
                            
                        }else{

                            if( true == that.visible ){
                                jQuery( that.selector ).fadeOut();
                                that.visible = false;
                            }
                        }
                    });

                    this.bindEvent();
                }
            }
        }

        this.bindEvent = function(){
            jQuery( document ).on( 'click', this.selector, function(e){
                e.preventDefault();
                $("html, body").animate({ scrollTop: 0 }, 800);
            });
        }

        this.getMarkup = function(){

            var position = this.fixed ? 'fixed':'absolute';

            var wrapperStyle = 'style="position: '+position+';z-index:999999; bottom: 20px; right: 20px;"';

            var buttonStyle  = 'style="cursor:pointer;display: inline-block;padding: 10px 20px;background: #f15151;color: #fff;border-radius: 2px;"';

            var markup = '<div ' + wrapperStyle + ' id="scroll-to-top"><span '+buttonStyle+'>Scroll To Top</span></div>';

            this.markup   = ( param && param.markup ) ? param.markup : markup;
        }

        this.valid = function(){

            if( param && param.markup && !param.selector ){
                alert( 'Please provide selector. eg. { markup: "<div id=\'scroll-top\'></div>", selector: "#scroll-top"}' );
                return false;
            }

            return true;
        }
    }

        //toggle Rating Form
    function toggleForm( param ){
        this.selector = param.selector, 
        this.init = function(){
            var _this = this;

            jQuery('#reply-title').on('click', function(e){
                e.preventDefault();
                _this.toggleform();
            });
        }

        this.toggleform = function(){
            jQuery(this.selector).fadeToggle();
        }
    }

    //on scroll fix header
    function fixOnScroll(param){
        this.selector = param.selector,
        this.init = function(){
            var _this = this;
            jQuery(_this.selector).each(function(){
                var wrapper = jQuery(this).find('.affix-top');
                var offsetTop = 90;

                $(document).ready(fixHeader);
                $(window).scroll(fixHeader);

                function fixHeader() {
                    
                    var currentScroll = $(window).scrollTop();
                    var win_width = $(window).width();
                    if (currentScroll > offsetTop && win_width > 991) {
                        if (!$(this).hasClass('position-fixed')) {
                                $('.site-header').addClass("position-fixed");
                                 $('.site-content').css('margin-top','200px');
                        }
                    }
                    else {
                        $('.site-header').removeClass("position-fixed");
                         $('.site-content').css('margin-top','0');
                    }
                }
            });
        }
    }

    // parallax title
    function fakeParallaxTitle(param){
        this.selector =  param.selector,
        this.init = function(){
            var _this = this;
            var win_width = $(window).width();
            if (win_width > 767) {
              $(window).scroll(function(){
                var wScroll = $(this).scrollTop();
                $(_this.selector).css({
                  'margin-top': wScroll / 2.25 +'px'
                });
              });
            }

        }
    }

    //toggle search wrapper
    function olgSearch( param ){
        this.searchEle = param.searchEle,
        this.enabled   = false,
        this.init      = function(){
            var that = this;

            $('#searchToggler').on('click', function(e){
                e.stopPropagation();
                that.enabled = true;
                $( that.searchEle ).slideDown(200);
                $( that.searchEle).find('input').focus();
            });

            $( document ).on('click touchstart', function(e){
                if ( $(e.target).closest(that.searchEle).length === 0 && that.enabled ) {
                    that.enabled = false;
                    $( that.searchEle ).slideUp(200);
                    $( that.searchEle).find('input').blur();
                }
            });
        }
    }

    function sideNav(param){
        this.opentoggler = param.opentoggler,
        this.closetoggler = param.closetoggler,
        this.toggleCart = param.toggleCart,
        this.animation = param.animation,
        this.init = function(){
            var _this = this;
            $(_this.opentoggler).on('click', function(e){
                e.preventDefault();
                $('body').addClass('shift-body-left');
                $(_this.toggleCart).addClass('open');
            });

            $(_this.closetoggler).on('click', function(e){
                e.preventDefault();
                $('body').removeClass('shift-body-left');
                $(_this.toggleCart).removeClass('open');
            })

        }
    }

    new preloader({
        time: 500
    }).init();

    $( document.body ).bind( 'added_to_cart', function( event, fragments, cart_hash ) {
        var $cartCounter = $('.cart-customlocation');
        var count = parseInt( $cartCounter.text() );
        count =  isNaN( count ) ? 0 : count;
        $cartCounter.text( ++count ); 
        $( '#olgasus-mini-cart-wrapper' ).html(fragments["div.widget_shopping_cart_content"]);
        $('body').addClass('shift-body-left');
        $('#olgasus-nav').addClass('open');
    });


        
    jQuery( window ).on( 'load',function( event, fragments, cart_hash ){
        if( localData.open_menu == 1){
            $( 'body' ).addClass( 'shift-body-left' );
            $( '#olgasus-nav' ).addClass( 'open' );
        }
    });

    jQuery( window ).on( 'load',function ( event,fragments, cart_hash){
        if( localData.remove_cart_data == 1){
            $( 'body' ).addClass( 'shift-body-left' );
            $( '#olgasus-nav' ).addClass( 'open' );
        }
    })

    $(document).ready(function(){

        olgCheckout.init();


        cpt = new changeProductThumbnail();
        cpt.apply();

        // hei = new changeProductThumbnail().thumbnailflow();

        /*Cart Custom Checkout Button*/
       jQuery('form[name=olgasus-checkout-select]').on('submit', function (e) {
            e.preventDefault();
           window.location.href = jQuery("input[name=payment]:checked").val();
        });

        new olgSearch({
            searchEle : '#search-wrapper'
        }).init();

        new sideNav({
            'opentoggler' : '#miniCartToggler',
            'closetoggler' : '.olgasus-nav-overlay, #mc-close',
            'toggleCart' : '#olgasus-nav'
        }).init();

        new load_product_details({
        }).init();

        new toggleForm({
            'selector': '#commentform'
        }).init();

        new scrollToTop({
            markup   : '<div id="scrollToTop"><span><i class="fa fa-angle-double-up"></i></span></div>',
            selector : '#scrollToTop'
        }).init();

       /* new fakeParallaxTitle({
            'selector': '.entry-header h1'
        }).init();*/

        new makeBackgroundImg({
            selector: '.inner-page-title, .home-banner'
        }).init();

        $(".woocommerce-main-image, .product-img-thumb").each(function() {
            $(this).css('background', ' #000 url("' + $(this).find('img').attr('src') + '") top center /cover' );
            $(this).find('img').css('visibility', 'hidden');
        });

        new fixOnScroll({
            'selector': '.site-header'
        }).init();
        // $('#accountToggler').on('click', function(){
        //     $('#user-account').toggleClass('open');
        // });

        var win_width = $(window).width();
        $('.prdctfltr_woocommerce_filter').on('click', function(){
            if (win_width < 991){
                // $('form.prdctfltr_woocommerce_ordering').slideToggle(1000);
                $('form.prdctfltr_woocommerce_ordering').toggleClass('togglethis');
            }
        });


        $('.prdctfltr_reset span').html('Rensa alla filter');

    });

    $( window ).load( function(){
        productThumbnailSlider();

        $( '.thumbnails' ).vslide({
            next : '.next',
            prev : '.prev',
            scroll : 200,
            height: 540
        });
    });


    /* Force select shipping method */
    var olgCheckout = {

        cartForm        : null,

        init: function(){
            var self = this;
            this.initializeForm(); 
            
            jQuery(document).on('submit','.shop_table.cart form',function(){
                self.updateMiniCartQuantiy();
                return false;
            });
        },

        initializeForm: function(){
            this.cartForm = jQuery( '.shop_table.cart form' );
        },

        updateMiniCartQuantiy: function(){
            var self = this;
            jQuery( '<input />' ).attr( 'type', 'hidden' )
                .attr( 'name', 'update_cart' )
                .attr( 'value', 'Update Cart' )
                .appendTo( this.cartForm );

            this.block( jQuery( '#olgasus-mini-cart-wrapper' ) );
            var formData = this.cartForm.serialize();
            
            jQuery.ajax( {
                type:     self.cartForm.attr( 'method' ),
                url:      self.cartForm.attr( 'action' ),
                data:     formData,
                dataType: 'html',
                success:  function(response){
                    wc_cart_fragment_url = (wc_cart_fragments_params.wc_ajax_url).replace("%%endpoint%%", "get_refreshed_fragments");
                    jQuery.ajax( {
                        type: 'post',
                        url:  wc_cart_fragment_url,
                        success:  function(response){
                            jQuery('#olgasus-mini-cart-wrapper').html(response.fragments['div.widget_shopping_cart_content']);
                            var count = 0;
                            jQuery( '.input-text' ).each( function( key, val ){
                              count += parseInt( $(this).val() );
                            });
                        jQuery( '.cart-customlocation' ).text( count );
                        },
                        complete: function() {
                            jQuery( '#olgasus-mini-cart-wrapper' ).unblock();
                            self.initializeForm();
                        }
                    } );
                }
            } );        
        },

        block : function(elem){
            elem.block( {
                message: null,
                overlayCSS: {
                    background: '#fff',
                    opacity: 0.6
                }
            } );
        }
    }


    /*Hide/SHow the displayed price after selection of variation*/
    var variation_wrap = jQuery( ".single_variation_wrap" );
    var variation_price = jQuery( ".rh-variation-price" );
    variation_wrap.on( "show_variation", function ( event, variation ) {
        variation_price.css( "display", "none");
    } );
    variation_wrap.on( "hide_variation", function ( event, variation ) {
        variation_price.css( "display", "inline-block");
    } ); 

    variation_price.closest('form').prev('div').css("display","none")

});


