<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package olgasus
 */
global $olgasus;
?>

	</div><!-- #content -->
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
			<div class = "footer-widget row">
				<!-- Footer 1 -->
				<div >
					<?php dynamic_sidebar('footer-1');?>
				</div>
				
				<!-- Footer 2 -->
				<div >
					<?php dynamic_sidebar('footer-2');?>
				</div>

				<!-- Footer 3 -->
				<div >
					<?php dynamic_sidebar('footer-3');?>
				</div>

				<!-- Footer 4 -->
				<div>
					<?php dynamic_sidebar('footer-4');?>
				</div>
			</div>
		</div>
		<div class="site-info">
			<?php if(isset($olgasus['footer-text'])):?>
				<?php echo $olgasus['footer-text'];?>
			<?php endif; ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
