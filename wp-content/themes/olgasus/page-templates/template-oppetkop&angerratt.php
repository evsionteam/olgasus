<?php

/*Template Name: OppetKop&Angerratt-page */
get_header(); ?>

	<!-- HEADER IMAGE AND TITLE -->
	<div class="inner-page-title">
		<h1><?php echo the_title(); ?></h1>
		<?php the_post_thumbnail('large'); ?>
	</div>
	<!-- # HEADER IMAGE AND TITLE -->
<?php 
	if(have_posts()):
		while(have_posts()):
			the_post();
		the_content();
		endwhile;
	endif;
	?>

<?php get_footer(); ?>