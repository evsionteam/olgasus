<?php
/**
 * Template Name: Brand
 */
get_header();

?>
<!-- HEADER IMAGE AND TITLE -->
<div class="inner-page-title">
	<h1>
		<?php
			the_title();
		?>
		
	</h1>
	<?php 
		the_post_thumbnail('large');
	 ?>
</div>
<!-- # HEADER IMAGE AND TITLE -->
	
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="page-content">
		<div class="container">

			<div class="brand-title"><?php echo get_post_meta( get_the_ID(), 'page_subtitle', true ); ?></div>
			<div class="brands-wrapper">
				<?php 

					echo uz_get_glossary_data(array(
					    'type'            	=> 'terms',
					    'taxonomy'        	=> 'brand',
					    'wrapper_class'		=> 'brands'	
					));
				?>
			</div>
		</div><!-- .container -->
	</div>
</article>
<?php get_footer(); 