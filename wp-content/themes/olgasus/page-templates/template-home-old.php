<?php
get_header();
/*Template Name: Homepage Old */
?>
	<!-- BANNER IMAGE -->
	<div class="home-banner">
		<?php if(isset($olgasus['banner-image']['url'])): ?>
			<img src="<?php echo $olgasus['banner-image']['url']; ?>" alt="banner-image" />
		<?php endif; ?>
	</div>
	<!--# BANNER IMAGE -->

	<section>
		<div class="page-content">
			<div class="page-hero">
				<!-- BANNER TITLE -->
				<?php if(isset($olgasus['banner-title'])): ?>
					<h1 class="page-hero-title"><?php echo $olgasus['banner-title']; ?></h1>
				<?php endif; ?>
				<!-- #BANNER TITLE -->
				<!-- BANNER INFO -->
				<?php if(isset($olgasus['banner-sub-title'])): ?>
					<p><?php echo $olgasus['banner-sub-title']; ?></p>
				<?php endif; ?>
				<!-- #BANNER INFO -->
			</div>


			<!-- SERVICES SECTION -->
			<div class="services-home">
				<div class="three-col">
				<?php if(isset($olgasus['field-i-title'])): ?>
					<h2 class="section-title"><?php echo $olgasus['field-i-title']; ?></h2>
				<?php endif; ?>
				
				<?php if(isset($olgasus['field-i-content'])): ?>
					<p><?php echo $olgasus['field-i-content']; ?></p>
				<?php endif; ?>
				</div>

				<div class="three-col">
				<?php if(isset($olgasus['field-ii-title'])): ?>
					<h2 class="section-title"><?php echo $olgasus['field-ii-title']; ?></h2>
				<?php endif; ?>
				

				<?php if(isset($olgasus['field-ii-content'])): ?>
					<p><?php echo $olgasus['field-ii-content']; ?></p>
				<?php endif; ?>
				</div>

				<div class="three-col">
				<?php if(isset($olgasus['field-iii-title'])): ?>
					<h2 class="section-title"><?php echo $olgasus['field-iii-title']; ?></h2>
				<?php endif; ?>

				<?php if(isset($olgasus['field-iii-content'])): ?>
					<p><?php echo $olgasus['field-iii-content']; ?></p>
				<?php endif; ?>
				</div>
			</div>	
			<!-- #SERVICES SECTION -->
			
		</div>
	</section>


	<!-- INSTAGRAM SECTION -->
	<div class="insta-holder page-content">
		<h2 class="section-title">INSTAGRAM</h2>
		<p>Följ oss på instagram och få senaste tipsen på våra kläder och inredning</p>

		<?php echo do_shortcode('[instagram-feed]'); ?>
	</div>
	<!-- #INSTAGRAM SECTION -->

<?php get_footer(); ?>