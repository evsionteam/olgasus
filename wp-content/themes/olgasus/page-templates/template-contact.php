<?php

/*Template Name: Contactpage */
get_header();
global $olgasus;
?>
	
	<!-- MAP -->
	<div class="inner-page-title">
		<h1><?php echo the_title(); ?></h1>

		<div id="olgasus-map" style="height:400px"></div>
	</div>
	<!-- #MAP -->
	
	<section>
		<div class="page-content">
			<h3 class="leave-message-title">LÄMNA ETT MEDDELANDE</h3>
			<!-- Contact Form -->
			<div class="contact-form">
				<?php echo do_shortcode('[contact-form-7 id="14" title="kontakt"]'); ?>
				
				<div class='social'>

					<?php if(!empty($olgasus['facebook-link'])){?>
					<a href='<?php echo $olgasus['facebook-link']; ?>' target='_blank' ><i class="fa fa-facebook-square" data-toggle="tooltip" data-placement="top" data-original-title="facebook"></i></a>
					<?php } ?>
					
					<?php if(!empty($olgasus['instagram-link'])){?>
					<a href='<?php echo $olgasus['instagram-link']; ?>' target='_blank' ><i class="fa fa-instagram"  data-toggle="tooltip" data-placement="top" data-original-title="instragram"></i></a>
					<?php } ?>

				</div>
			</div>
			<div class="contact-address">
				<div class="half">
					<?php if(isset($olgasus['title-1'])): ?>
						<b><?php echo $olgasus['title-1']; ?></b>
					<?php endif; ?>
					<br>
					<?php if(isset($olgasus['fulladdress-1'])): ?>
						<?php echo $olgasus['fulladdress-1']; ?>
					<?php endif; ?>
					<br><br>
					<?php if(isset($olgasus['timing-1'])): ?>
						<?php echo $olgasus['timing-1']; ?>
					<?php endif; ?>
					<br><br>
					<?php if(isset($olgasus['telephone-1'])): ?>
						<?php echo $olgasus['telephone-1']; ?>
					<?php endif; ?>
					<br>
					<?php if(isset($olgasus['mail-1'])): ?>
						<?php echo $olgasus['mail-1']; ?>
					<?php endif; ?>
				</div>

				<div class="half">
					<?php if(isset($olgasus['title-2'])): ?>
						<b><?php echo $olgasus['title-2']; ?></b>
					<?php endif; ?>
					<br>
					<?php if(isset($olgasus['fulladdress-2'])): ?>
						<?php echo $olgasus['fulladdress-2']; ?>
					<?php endif; ?>
					<br><br>
					<?php if(isset($olgasus['timing-2'])): ?>
						<?php echo $olgasus['timing-2']; ?>
					<?php endif; ?>
					<br><br>
					<?php if(isset($olgasus['telephone-2'])): ?>
						<?php echo $olgasus['telephone-2']; ?>
					<?php endif; ?>
					<br>
					<?php if(isset($olgasus['mail-2'])): ?>
						<?php echo $olgasus['mail-2']; ?>
					<?php endif; ?>
				</div>
				
			</div>
		</div>
	</section>

	
<?php get_footer(); ?>