<?php

/*Template Name: Omm Os-page */
get_header(); ?>

	<!-- HEADER IMAGE AND TITLE -->
	<div class="inner-page-title">
		<h1><?php echo the_title(); ?></h1>
		<?php the_post_thumbnail('large'); ?>
	</div>
	<!-- # HEADER IMAGE AND TITLE -->

<?php 
	if(have_posts()):
		while(have_posts()):
			the_post();
		the_content();
		endwhile;
	endif;
	?>

	<div class="page-content">
		<div class="desc-holder">
			<h2 class="section-title">KONTAKTA OSS</h2>
			
			<div class="contact-portion">
				<div class="half">
					<?php if(isset($olgasus['title-1'])): ?>
						<b><?php echo $olgasus['title-1']; ?></b>
					<?php endif; ?>
					<br>
					<?php if(isset($olgasus['fulladdress-1'])): ?>
						<?php echo $olgasus['fulladdress-1']; ?>
					<?php endif; ?>
					<br><br>
					<?php if(isset($olgasus['timing-1'])): ?>
						<?php echo $olgasus['timing-1']; ?>
					<?php endif; ?>
					<br><br>
					<?php if(isset($olgasus['telephone-1'])): ?>
						<?php echo $olgasus['telephone-1']; ?>
					<?php endif; ?>
					<br>
					<?php if(isset($olgasus['mail-1'])): ?>
						<?php echo $olgasus['mail-1']; ?>
					<?php endif; ?>
				</div>

				<div class="half">
					<?php if(isset($olgasus['title-2'])): ?>
						<b><?php echo $olgasus['title-2']; ?></b>
					<?php endif; ?>
					<br>
					<?php if(isset($olgasus['fulladdress-2'])): ?>
						<?php echo $olgasus['fulladdress-2']; ?>
					<?php endif; ?>
					<br><br>
					<?php if(isset($olgasus['timing-2'])): ?>
						<?php echo $olgasus['timing-2']; ?>
					<?php endif; ?>
					<br><br>
					<?php if(isset($olgasus['telephone-2'])): ?>
						<?php echo $olgasus['telephone-2']; ?>
					<?php endif; ?>
					<br>
					<?php if(isset($olgasus['mail-2'])): ?>
						<?php echo $olgasus['mail-2']; ?>
					<?php endif; ?>
				</div>		
			</div>
			
		</div>
	</div>


<?php get_footer(); ?>