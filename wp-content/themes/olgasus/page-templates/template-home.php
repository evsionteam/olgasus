<?php
get_header();
/*Template Name: Homepage */
?>

	<section>
		<div class="page-content">
			<!-- SERVICES SECTION -->
			<div class="services-home">
				<div class="three-col">
					<span class="olg-step-circle">1</span>
				<?php if(isset($olgasus['field-i-title'])): ?>
					<h2 class="section-title"><?php echo $olgasus['field-i-title']; ?></h2>
				<?php endif; ?>
				
				<?php if(isset($olgasus['field-i-content'])): ?>
					<p><?php echo $olgasus['field-i-content']; ?></p>
				<?php endif; ?>
				</div>

				<div class="three-col">
					<span class="olg-step-circle">2</span>
				<?php if(isset($olgasus['field-ii-title'])): ?>
					<h2 class="section-title"><?php echo $olgasus['field-ii-title']; ?></h2>
				<?php endif; ?>
				

				<?php if(isset($olgasus['field-ii-content'])): ?>
					<p><?php echo $olgasus['field-ii-content']; ?></p>
				<?php endif; ?>
				</div>

				<div class="three-col">
					<span class="olg-step-circle">3</span>
				<?php if(isset($olgasus['field-iii-title'])): ?>
					<h2 class="section-title"><?php echo $olgasus['field-iii-title']; ?></h2>
				<?php endif; ?>

				<?php if(isset($olgasus['field-iii-content'])): ?>
					<p><?php echo $olgasus['field-iii-content']; ?></p>
				<?php endif; ?>
				</div>
			</div>	
			<!-- #SERVICES SECTION -->
		</div>
	</section>

	<!-- INSTAGRAM SECTION -->
	<div class="insta-holder page-content">
		<h2 class="section-title has-line-border"><span><?php echo esc_html__( 'INSTAGRAM', 'olgasus' ); ?></span></h2>
		<p><?php echo $olgasus['home-insta-textarea']; ?></p>
		<?php echo do_shortcode('[instagram-feed cols="4" num=12]'); ?>

		
		<div class="page-content insta-form-wrapper">			
			<?php 
				$short_code = $olgasus['home-contact-form'];
				echo do_shortcode($short_code); ?>
		</div>
		
	</div>
	<!-- #INSTAGRAM SECTION -->

<?php get_footer(); ?>